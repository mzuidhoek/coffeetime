﻿using System;

using Xamarin.Forms;

namespace CoffeeTime
{
	public class App : Application
	{
		public App (Page startPage)
		{
			// The root page of your application
			MainPage = startPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

